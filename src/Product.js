import React from 'react';
import Config from './config.js';
import pic1 from './k&r.jpg';
import pic2 from './Pierce.jpg';
import pic3 from './OReilly.jpg';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/esm/Col';
import Rating from 'react-rating'
import { Star, StarFill } from 'react-bootstrap-icons';
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'

class Product extends React.Component {
    constructor(props) {
        super(props);
        const { productId } = this.props.match.params;
        this.state = {
            productId: productId,
            name: "",
            metadata : {}
        }
    }

    componentDidMount() {
        fetch(Config.hostname + "/product/" + this.state.productId)
            .then(res => res.json())
            .then(
                (res) => {
                    this.setState({
                        name: res.Item.ProductName,
                        metadata: Config.PRODUCT_TO_METADATA[ this.state.productId ]
                    })
                },
                (err) => {
                    console.log(err);
                }
            )
    }


    render() {
        return (
            <div>
                <br/>
                <br/>
                 <Container fluid="lg">
                     <Card border="light">
                        <Card.Body>
                        <Card.Title style={{ fontSize: '3.25em' }}>{this.state.name}</Card.Title>
                        <br/>
                        <Container>
                            <Row>
                                <Col md={5}>
                                    <Card.Img variant="right rounded" src={this.state.metadata.img}/>
                                </Col>
                                <Col md={7}>
                                <Rating
                                    placeholderRating={this.state.metadata.rating}
                                    
                                    emptySymbol={<Star size={22} className="ml-2"></Star>}
                                    placeholderSymbol={<StarFill size={22} className="ml-2"></StarFill>}
                                    fullSymbol={<StarFill size={22} className="ml-2"></StarFill>}
                                />
                                <br/>
                                <br/>
                                <Button variant="success" onClick={this.addToCart.bind(this)}>Add to Cart</Button>
                                <br/>
                                <br/>
                                <Table responsive="sm">
                                <tbody>
                                <tr>
                                    <td><b>Price</b></td>
                                    <td>{this.state.metadata.price}</td>
                                </tr>
                                <tr>
                                    <td><b>Author</b></td>
                                    <td>{this.state.metadata.author}</td>
                                </tr>
                                <tr>
                                    <td><b>Details</b></td>
                                    <td>{this.state.metadata.description}</td>
                                </tr>
                                </tbody>
                                </Table>
                                </Col>
                            </Row>
                        </Container>
                        </Card.Body>
                     </Card>
                 </Container>
            </div>
        )
    }

    addToCart() {

        const requestOptions = {
            method: "POST",
        };
        const { productId } = this.state;
        const customerId = localStorage.getItem("customerId");
        fetch(Config.hostname + "/cart/" + customerId + "/add/" + productId, requestOptions)
            .then(res => res.json())
            .then(
                (res) => {
                    console.log(res);
                    alert("Added to cart!")
                }
            )
    }
}

export default Product;
