import React from 'react';
import Config from './config.js';
import { Link } from 'react-router-dom'
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/esm/Col';
import pic1 from './k&r.jpg';
import pic2 from './Pierce.jpg';
import pic3 from './OReilly.jpg';


class Home extends React.Component {
    static PRODUCT_TO_DISPLAY_NAME = {
        "97b513ab-84f4-4d4f-b6fe-db8e0803789e": "Types & Programming Languages",
        "f0d36b99-1e77-4e58-85d4-64c05d4a20fd": "The C Programming Language",
        "f239b1b5-fb20-4812-856a-44a99970f52a": "Think Python",
    }


    render() {
        const productsDisplay = Object.entries(Home.PRODUCT_TO_DISPLAY_NAME)
            .map(([productId, name]) => (<tr><Link to={"/product/" + productId}>{name}</Link></tr>));


        const productCards = [];
        for (const [productId, metadata] of Object.entries(Config.PRODUCT_TO_METADATA)) {
            productCards.push(
                <div>
                    <Card border="light">
                        <Card.Body>
                            <Container>
                                <Row>
                                    <Col xs={2}>
                                        <Card.Img variant="right rounded" src={metadata.img} height="200" />
                                    </Col>
                                    <Col>
                                        <Card.Title><Link to={"/product/" + productId}>{metadata.name}</Link></Card.Title>
                                        <Card.Text>
                                            {metadata.description}
                                        </Card.Text>
                                    </Col>
                                </Row>
                            </Container>
                        </Card.Body>
                    </Card>
                    <br />
                    <br />
                </div>
            );
        }

        return (
            <div>
                <br />
                <br />
                <Container fluid="lg">
                    {productCards}
                </Container>
            </div>
        );
    }
}

export default Home;
