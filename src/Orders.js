import React from 'react';
import Config from './config.js';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Table from 'react-bootstrap/Table';

class Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: []
        };
    }

    async componentDidMount() {
        const customerId = localStorage.getItem("customerId");
        const orders = await fetch(Config.hostname + "/orders/" + customerId).then(res => res.json());
        this.setState({
            orders: orders.Orders,
        })
    }

    render() {
        const displayOrders = [];
        for (const order of this.state.orders) {
            displayOrders.push(
                <tr>
                <td>{order.OrderId}</td>
                </tr>
            )
        }


        return (
            <div>
                <br/>
                <br/>
                 <Container fluid="lg">
                     <Card border="light">
                        <Card.Body>
                        <Card.Title style={{ fontSize: '3.25em' }}>Your Orders</Card.Title>
                        <br/>
                        <Container>
                            <Table responsive="md">
                                <tbody>
                                {displayOrders}
                                </tbody>
                            </Table>
                        </Container>
                        </Card.Body>
                     </Card>
                     </Container>
            </div>
        )
    }
}

export default Orders;
